//
//  ViewControllerTests.swift
//  TopMoviesMVVMTests
//
//  Created by Juan Calvo on 8/17/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit
import XCTest
@testable import TopMoviesMVVM

class ViewControllerTests: XCTestCase {
    var controller: TopMoviesViewController!
    
    override func setUp() {
        super.setUp()
         let storyBoard = UIStoryboard(name: "Main", bundle: Bundle(for: type(of: self)))
        if let vController = storyBoard.instantiateViewController(withIdentifier:
            "TopMoviesViewController") as? TopMoviesViewController {
            controller = vController
            controller.loadView()
            controller.viewDidLoad()
        } else {
            XCTFail("Error instantiating TopMoviesViewController")
        }
    }
    
    override func tearDown() {
        controller = nil
        super.tearDown()
    }
    
    func testTables() {
        XCTAssert((controller?.moviesTable.numberOfSections)! > 0)
    }
    
    func testNumberOfViews() {
        XCTAssert((controller?.view.subviews.count)! > 0)
    }
}
