//
//  MoviesClient.swift
//  TopMoviesMVVM
//
//  Created by Juan Calvo on 7/2/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit

class MoviesClient: NSObject {
    func fetchMovies(completion: @escaping ([NSDictionary]?) -> ()) {
        let url = URL(string: "http://ax.itunes.apple.com/WebObjects/MZStoreServices.woa/ws/RSS/topMovies/json")!
        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: url) { (data, response, error) in
            if error != nil {
                completion(nil)
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .allowFragments) as? NSDictionary
                if let movies = json?.value(forKeyPath: "feed.entry") as? [NSDictionary] {
                    completion(movies)
                    return
                }
            } catch {
                completion(nil)
                return
            }
        }
        task.resume()
    }
}
