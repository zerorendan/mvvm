//
//  TopMoviesViewController.swift
//  TopMoviesMVVM
//
//  Created by Juan Calvo on 8/17/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit

class TopMoviesViewController: UIViewController {
    @IBOutlet var viewModel: ViewModel!
    @IBOutlet var moviesTable: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel.fetchMovies {
            DispatchQueue.main.async { () -> Void in
                self.moviesTable.reloadData()
            }
        }
    }
}

extension TopMoviesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItemInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as UITableViewCell
        configure(Cell: cell, IndexPath: indexPath)
        return cell
    }
    
    private func configure(Cell cell:UITableViewCell, IndexPath indexPath:IndexPath) {
        cell.textLabel?.text = viewModel.titleForIndexPath(indexPath: indexPath)
    }
}

