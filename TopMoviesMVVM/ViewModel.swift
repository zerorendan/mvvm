//
//  ViewModel.swift
//  TopMoviesMVVM
//
//  Created by Juan Calvo on 7/2/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit

class ViewModel: NSObject {
    @IBOutlet var moviesClient: MoviesClient!
    var movies: [NSDictionary]?
    
    func fetchMovies(completion: @escaping () -> ()) {
        moviesClient.fetchMovies { movies in
            self.movies = movies
            completion()
        }
    }
    
    func numberOfItemInSection(section: Int) -> Int {
        return movies?.count ?? 0
    }
    
    func titleForIndexPath(indexPath:IndexPath) -> String {
        return movies![indexPath.row].value(forKeyPath: "im:name.label") as? String ?? ""
    }
}
